import requests

def url(page):
    page_num = str(page).zfill(4)
    return 'https://catalog.mcgraw-hill.com/repository/protected_content/RALLY_BOOK/50002057/74/53/OPS/images/page'+ str(page_num) +'.svgz'

headers = {
    'authority': 'catalog.mcgraw-hill.com',
    'upgrade-insecure-requests': '1',
    'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36',
    'sec-fetch-mode': 'no-cors',
    'accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3',
    'sec-fetch-site': 'same-origin',
    'referer': 'https://catalog.mcgraw-hill.com/repository/protected_content/RALLY_BOOK/50002057/74/53/OPS/page0012.xhtml',
    'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9,el;q=0.8,la;q=0.7',
    'cookie': 'CloudFront-Key-Pair-Id=APKAJKLS43KR7BU353QA; CloudFront-Policy=eyJTdGF0ZW1lbnQiOiBbeyJSZXNvdXJjZSI6Imh0dHBzOi8vY2F0YWxvZy5tY2dyYXctaGlsbC5jb20vcmVwb3NpdG9yeS9wcm90ZWN0ZWRfY29udGVudC9SQUxMWV9CT09LLzUwMDAyMDU3Lzc0LzUzLyoiLCJDb25kaXRpb24iOnsiRGF0ZUxlc3NUaGFuIjp7IkFXUzpFcG9jaFRpbWUiOjE1NjY1Mjg2NjZ9fX1dfQ__; CloudFront-Signature=f0lgeiQMiofzqeWuABXm5GgDqVdZ1KF1OMIU7ngAQSGTXOt0Dmvq2VyX56YwWYZF-XHQGndNo~qq4YA19vP2YM-~QJu5BS~~2Ojsyl3WzqhFY7xkWWDapG7B6Xy-p62d5uU8mNJpd5tzwtegEvOcNS1An38gqqnAOijFq9fpjcqp8Mkty8BJXxi6aB9nnPUDUKHCTet-retn4X7zdVSns7Yi0yNWWNopdESVWHvVXrlIU7MfLv31p0hkw5AFmyWaWC5PYseV2HaS35203ZsgC2YrVsxcdzg4K3rCGXtJ~bkF5r-Vv11o9MqLkdQW60SVhnODkY-TxK02SMQ3wdXFcw__; JSESSIONID=59DE31BBC9F75D6DB97609305C7F6B91; PORTALLB=3701873324.55303.0000',
}

 

for page in range(1200):
    with open('pages/'+ str(page) +'.svg', 'w') as file:
        file.write(requests.get(url(page), headers=headers).text)
        print(page)