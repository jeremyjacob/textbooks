import cairosvg, os
cairosvg.svg2pdf(url='pages/104.svg', write_to='image.pdf')

for file in os.listdir('pages'):
    print(file.split('.')[0] + '.pdf')
    try:
        cairosvg.svg2pdf(url='pages/' + file, write_to='pdfs/' + file.split('.')[0] + '.pdf')
        print('writing '+file)
    except:
        pass